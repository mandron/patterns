<h1>Цепочка обязанностей</h1>

<?php

interface iParser {
    public function __construct(Parser $parser);
    public function parse($data);
    public function getSuccessor();
    public function canHandle($data, $needle);
}

class Parser implements iParser {
    
    protected $successor;
    
    public function __construct(Parser $successor = NULL) {
        $this->successor = $successor;
    }

    public function parse($data) {
        $successor = $this->getSuccessor();
        if (!is_null($successor)) {
            $successor->parse($data);
        } else {
            var_dump("Unable to find correct parser");
        }
    }
    
    public function canHandle($data, $needle){
        if ($data === $needle) {
            return true;
        }
        return false;
    }

    public function getSuccessor() {
        return $this->successor;
    }

}

class aParser extends Parser {
    
    public function parse($data) {
        if ($this->canHandle($data, 'a')) {
            var_dump("This is '{$data}'");
        } else {
            parent::parse($data);
        }
    }
    
}

class bParser extends Parser {
    
    public function parse($data) {
        if ($this->canHandle($data, 'b')) {
            var_dump("This is '{$data}'");
        } else {
            parent::parse($data);
        }
    }
    
}

class cParser extends Parser {
    
    public function parse($data) {
        if ($this->canHandle($data, 'c')) {
            var_dump("This is '{$data}'");
        } else {
            parent::parse($data);
        }
    }
    
}

class ParserProcessor {
    public static function run($data) {
        $a_parser = new aParser();
        $b_parser = new bParser($a_parser);
        $c_parser = new cParser($b_parser);
        
        $c_parser->parse($data);
    }
}

ParserProcessor::run('a');
ParserProcessor::run('b');
ParserProcessor::run('c');
ParserProcessor::run('d');
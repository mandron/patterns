<h1>Одиночка (усложненная задача)</h1>

<p>Создать абстрактный класс, от которого все наследники получают функционал одиночки, 
    при этом объекты должны создаваться своего класса, а не класса родителя</p>
<?php

abstract class Singleton {
    
    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}
    
    protected static $_instances = array();
    
    public static function getInstance() {
        $class = get_called_class();
        
        if (!isset(static::$_instances[$class])) {
            static::$_instances[$class] = new $class();
        }
        return static::$_instances[$class];
    }
    
}

class A extends Singleton {
    
}

class B extends Singleton {
    
}

$singleA = A::getInstance();
var_dump($singleA);

$singleB = B::getInstance();
var_dump($singleB);
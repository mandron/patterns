<h1>Хранитель</h1>

<?php

class Memento {
    
    private $count;
    private $name;
    
    function __construct(Originator $object) {
        $this->count = $object->getCount();
        $this->name = $object->getName();
    }
    
    function getCount() {
        return $this->count;
    }

    function getName() {
        return $this->name;
    }
    
}

class Caretaker {
    private $memento;
    
    public function setMemento($memento) {
        $this->memento = $memento;
    }
    
    public function getMemento() {
        return $this->memento;
    }
}

class Originator {
    
    private $count = 0;
    private $name = 'one';
    
    public function setMemento(Memento $memento) {
        $this->count = $memento->getCount();
        $this->name = $memento->getName();
    }
    
    public function createMemento() {
        return new Memento($this);
    }
    
    function getCount() {
        return $this->count;
    }

    function getName() {
        return $this->name;
    }

    function setCount($count) {
        $this->count = $count;
    }

    function setName($name) {
        $this->name = $name;
    }
    
}

$orig = new Originator();
echo '1 начальные настройки';
var_dump($orig);

echo '2 изменение настроек';
$orig->setCount(20);
$orig->setName('John Doe');
var_dump($orig);

echo '3 сохранение настроек';
$caretaker = new Caretaker();
$caretaker->setMemento($orig->createMemento());
var_dump($caretaker);

echo '4 изменение настроек';
$orig->setCount(10);
$orig->setName('two two');
var_dump($orig);

echo '5 восстановление настроек';
$orig->setMemento($caretaker->getMemento());
var_dump($orig);
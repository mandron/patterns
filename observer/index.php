<h1>Наблюдатель</h1>

<?php

interface iObservable {
    public function attach(iObserver $observer, $event);
    public function detach(iObserver $observer, $event);
    public function notify($event);
}

interface iObserver {
    public function update();
}

abstract class Observable implements iObservable {
    
    protected $observers = [];
    
    public function attach(iObserver $observer, $event) {
        if (!isset($this->observers[$event])) {
            $this->observers[$event] = [];
        }
        $this->observers[$event][] = $observer;
    }

    public function detach(iObserver $observer, $event) {
        if (isset($this->observers[$event])) {
            $data = [];
            foreach($this->observers[$event] as $ob) {
                if ($observer !== $ob) {
                    $data[] = $ob;
                }
            }
            if (empty($data)) {
                unset($this->observers[$event]);
            } else {
                $this->observers[$event] = $data;
            }
        }
    }

    public function notify($event) {
        if (isset($this->observers[$event])) {
            foreach($this->observers[$event] as $ob) {
                $ob->update();
            }
        }
    }
    
}

class observerA implements iObserver {
    
    public function update() {
        var_dump('Оповещение '.__CLASS__);
    }

}

class observerB implements iObserver {
    
    public function update() {
        var_dump('Оповещение '.__CLASS__);
    }

}

class A extends Observable {
    
}

$a = new A();
$observerA = new observerA();
$observerB = new observerB();

var_dump('1 Добавляем наблюдателя А на событие start');
$a->attach($observerA, "start");
var_dump($a);

var_dump('2 Добавляем наблюдателя B на событие finish');
$a->attach($observerB, "finish");
var_dump($a);

var_dump('3 Добавляем наблюдателя B на событие start');
$a->attach($observerB, "start");
var_dump($a);

var_dump('4 Запускаем событие start');
$a->notify('start');

var_dump('5 Запускаем событие finish');
$a->notify('finish');

var_dump('6 Удаляем наблюдателя B из события finish');
$a->detach($observerB, 'finish');
var_dump($a);

var_dump('7 Удаляем наблюдателя B из события start');
$a->detach($observerB, 'start');
var_dump($a);

var_dump('8 Удаляем наблюдателя A из события start');
$a->detach($observerA, 'start');
var_dump($a);
<?php

$folders = glob('*', GLOB_ONLYDIR);

?>

<ul>
<?php foreach($folders as $folder) : ?>

    <li><a href="/<?=$folder?>/"><?=$folder?></a></li>

<?php endforeach; ?>
</ul>
<h1>Строитель</h1>

<?php


interface iBuilder {
    public function execute();
}


class Builder implements iBuilder {
    
    protected $select = '';
    protected $where = '';
    protected $from = '';
    
    public function __construct() {
        return $this;
    }

    
    public function execute() {
        return implode(' ', [
            implode(' ', ['SELECT', $this->select]),
            implode(' ', ['FROM', $this->from]),
            (!empty($this->where) ? implode(' ', ['WHERE', $this->where]) : NULL),
        ]);
    }
    
    public function select($select) {
        $this->select = $select;
        return $this;
    }

    public function where($where) {
        $this->where = $where;
        return $this;
    }

    public function from($from) {
        $this->from = $from;
        return $this;
    }
    
}

$builder = (new Builder())
        ->where('id=1')
        ->from('table, table2')
        ->select('id');

var_dump($builder->execute());
var_dump($builder);